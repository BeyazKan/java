import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class BasitTablo {

	public static void main(String[] args) {
		String[][] ulkeler = {
				{"Japonya" , "245"},
				{"ABD" , "240"},
				{"İtalya" , "220"},
				{"İspanya" , "217"},
				{"Türkiye" , "215"},
				{"İngiltere" , "214"},
				{"Fransa" , "190"},
				{"Yunanistan" , "185"},
				{"Almanya" , "180"},
				{"Portekiz" , "170"}
		};
		
		String[] baslik = {"Ülke", "Televizyon İzleme gün/dk"};
		
		JFrame f = new JFrame("Tablo Örneği");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JTable table = new JTable(ulkeler, baslik);
		f.add(new JScrollPane(table));
		f.pack();
		f.setVisible(true);

	}

}
