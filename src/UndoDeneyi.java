import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.undo.UndoManager;

public class UndoDeneyi {

	public static void main(String[] args) {
		JFrame f = new JFrame();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		final JTextArea textarea = new JTextArea(20, 40);
		textarea.setText("Buraya Dön");
		f.add(new JScrollPane(textarea));
		
		final UndoManager undomaganer = new UndoManager();
		textarea.getDocument().addUndoableEditListener(undomaganer);
		undomaganer.setLimit(500);
		
		JButton undoDugmesi = new JButton("Undo");
		undoDugmesi.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				undomaganer.end();
				if(undomaganer.canUndo())
					undomaganer.undo();
				textarea.requestFocus();				
			}
		});
		
		f.add(undoDugmesi, BorderLayout.PAGE_END);
		f.pack();
		f.setVisible(true);
	}

}
