import java.awt.Dimension;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;

public class MenuCubugu {

	public static void main(String[] args) {
		JFrame frame = new JFrame("Menu Çubuğu");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JMenuBar menuCubugu = new JMenuBar();
		
		JMenu jmenu = new JMenu("Menü");
		
		JCheckBoxMenuItem item1 = new JCheckBoxMenuItem("İtem 1");
		jmenu.add(item1);
		
		jmenu.add(new JSeparator());
		
		JMenuItem item2 = new JMenuItem("İtem 2");
		jmenu.add(item2);
		
		menuCubugu.add(jmenu);
		frame.setJMenuBar(menuCubugu);
		
		frame.setSize(new Dimension(400, 300));
		frame.setVisible(true);

	}

}
