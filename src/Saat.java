import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.Timer;

public class Saat extends JFrame {
	
	private JTextField saatAlani;
	
	public Saat(){
		saatAlani = new JTextField(5);
		saatAlani.setEditable(false);
		
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout());
		panel.add(saatAlani);
		
		this.setContentPane(panel);
		this.pack();
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		
		Timer t = new Timer(1000, new SaatListener());
		
		t.start();
	}
	
	class SaatListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e){
			Calendar simdikiZaman = Calendar.getInstance();
			int saat  = simdikiZaman.get(Calendar.HOUR_OF_DAY);
			int dakika = simdikiZaman.get(Calendar.MINUTE);
			int saniye = simdikiZaman.get(Calendar.SECOND);
			
			saatAlani.setText("" + saat + ":" + dakika + ":" + saniye);
		}
		
	}
	
	public static void main(String[] args) {
		JFrame clock = new Saat();
		
		clock.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		clock.setVisible(true);

	}

}
