import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JTextField;

public class TextField {

	public static void main(String[] args) {
		JFrame frame = new JFrame("TextField Örneği");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		final JTextField metinGirisi = new JTextField("Buraya bilgi gir", 30);
		
		metinGirisi.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				metinGirisi.setText("OK");
				
			}
		});
		
		frame.add(metinGirisi);
		frame.pack();
		frame.setVisible(true);

	}

}
