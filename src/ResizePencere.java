import java.awt.Toolkit;

import javax.swing.JFrame;

public class ResizePencere {

	public static void main(String[] args) {
		JFrame f = new JFrame("Turkiye");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		f.setSize(Toolkit.getDefaultToolkit().getScreenSize());
		f.setLocation(0, 0);
		f.setVisible(true);
		f.setResizable(false);

	}

}
