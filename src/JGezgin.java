import java.io.IOException;
import java.net.URL;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

public class JGezgin extends JEditorPane implements HyperlinkListener{
	
	JGezgin(String url){
		setEditable(false);
		addHyperlinkListener(this);
		try {
			setPage(new URL(url));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void hyperlinkUpdate(HyperlinkEvent event){
		HyperlinkEvent.EventType typ = event.getEventType();
		
		if(typ == HyperlinkEvent.EventType.ACTIVATED){
			try {
				setPage(event.getURL());
			} catch (IOException e) {
				JOptionPane.showMessageDialog(this, "Linki takip edemiyorum : " + event.getURL().toExternalForm(), "Yükleme Hatası", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	
	public static void main(String[] args) {
		JFrame frame = new JFrame("Gezgin");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frame.setSize(600, 500);
		frame.add(new JScrollPane(new JGezgin("http://www.google.com")));
		frame.setVisible(true);

	}

}
