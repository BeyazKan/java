import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPasswordField;

public class PasswordField {

	public static void main(String[] args) {
		JFrame frame = new JFrame("PasswordField Örneği");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		final JPasswordField metinGirisi = new JPasswordField(20);
		//metinGirisi.setEchoChar('#');
		metinGirisi.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				metinGirisi.setText("OK");
				
			}
		});
		frame.add(metinGirisi);
		frame.pack();
		frame.setVisible(true);

	}

}
