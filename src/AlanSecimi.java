import java.awt.BorderLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JFrame;

public class AlanSecimi {

	public static void main(String[] args) {
		JFrame f = new JFrame("Sinema Filimleri");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Icon secilidegil = new ImageIcon("icon/cancel.png");
		Icon secili = new ImageIcon("icon/ok.png");
		
		JCheckBox jcheckbox1 = new JCheckBox("Komedi", true);
		jcheckbox1.setIcon(secilidegil);
		jcheckbox1.setSelectedIcon(secili);
		f.add(jcheckbox1, BorderLayout.PAGE_START);
		
		JCheckBox jcheckbox2 = new JCheckBox("Duygusal", false);
		jcheckbox2.setIcon(secilidegil);
		jcheckbox2.setSelectedIcon(secili);
		f.add(jcheckbox2, BorderLayout.PAGE_END);
		
		ItemListener itemlistener = new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				System.out.println(((JCheckBox)e.getItem()).getText());
				System.out.println(e.getStateChange() == ItemEvent.SELECTED ? "secili" : "secili değil");
				
			}
		};
		
		jcheckbox1.addItemListener(itemlistener);
		jcheckbox2.addItemListener(itemlistener);
		
		f.pack();
		f.setVisible(true);

	}

}
