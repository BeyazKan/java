import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class ModelListBoks {

	public static void main(String[] args) {
		JFrame frame =  new JFrame("Ülkeler");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		final DefaultListModel model = new DefaultListModel();
		
		for(String s : ("1. Türkiye,2. Almanya,3. Fransa,4. İtalya,5. İngiltere,6. Arabistan,7. Son").split(","))
			model.addElement(s);
		
		JList list = new JList(model);
		
		list.addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if(e.getValueIsAdjusting())
					return;
				System.out.println(e);
				
				if("7. Son".equals(model.get(e.getLastIndex())))
					System.exit(0);
				
			}
		});
		
		frame.add(new JScrollPane(list));
		frame.pack();
		frame.setVisible(true);

	}

}
