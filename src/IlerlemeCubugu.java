import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;

public class IlerlemeCubugu {
	
	static JProgressBar ilerlemeCubugu = new JProgressBar(0, 1000000);
	
	static class BaslatActionListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			new Thread(new Runnable(){
				public void run(){
					for(int i = 1; i <= ilerlemeCubugu.getMaximum(); ++i){
						final int j = i;
						SwingUtilities.invokeLater(new Runnable(){
							public void run(){
								ilerlemeCubugu.setValue(j);
							}
						});
					}
				}
			}).start();
		}
	}

	public static void main(String[] args) {
		JFrame f = new JFrame();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JButton jbutton = new JButton("Başlat");
		jbutton.addActionListener(new BaslatActionListener());
		ilerlemeCubugu.setStringPainted(true);
		f.add(ilerlemeCubugu, BorderLayout.NORTH);
		f.add(jbutton, BorderLayout.SOUTH);
		f.pack();
		f.setVisible(true);
		

	}

}
