import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JSpinner;
import javax.swing.SpinnerListModel;
import javax.swing.SpinnerNumberModel;

public class Spinner {

	public static void main(String[] args) {
		JFrame frame = new JFrame("Spinner Örneği");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));
		
		SpinnerNumberModel model1 = new SpinnerNumberModel(15.0, 10.0, 150.0, 0.2);
		
		JSpinner spinner1 = new JSpinner(model1);
		
		frame.add(spinner1);
		
		String[] sehir = {
				"Konya",
				"Balıkesir",
				"İstanbul",
				"Ankara",
				"İzmir"
		};
		
		SpinnerListModel model2 = new SpinnerListModel(sehir);
		JSpinner spinner2 = new JSpinner(model2);
		
		frame.add(spinner2);
		frame.setSize(300, 100);
		frame.setVisible(true);

	}

}
