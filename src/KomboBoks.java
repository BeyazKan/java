import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;

public class KomboBoks {

	public static void main(String[] args) {
		JFrame frame = new JFrame("ComboBox Örneği");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		String[] ulke = {
				"Türkiye",
				"Almanya",
				"İngiltere",
				"Fransa",
				"Amerika",
				"Son"
		};
		
		JComboBox combo1 = new JComboBox();
		
		for(String s : ulke)
			combo1.addItem(s);
		
		combo1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println(e);
				JComboBox seciliEleman = (JComboBox)e.getSource();
				
				if("Son".equals(seciliEleman.getSelectedItem())){}
					System.exit(0);
			}
		});
		
		frame.add(combo1, BorderLayout.WEST);
		
		JComboBox combo2 = new JComboBox(ulke);
		combo2.setEditable(true);
		combo2.setSelectedItem("Almanya");
		combo2.setMaximumRowCount(4);
		
		frame.add(combo2, BorderLayout.EAST);
		
		frame.pack();
		frame.setVisible(true);

	}

}
