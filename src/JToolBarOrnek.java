import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class JToolBarOrnek {

	public static void main(String[] args) {
		
		JFrame f = new JFrame("ToolBar Örnek");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		try { 
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
			} catch (ClassNotFoundException ex) { 
				Logger.getLogger(JToolBarOrnek.class.getName()).log(Level.SEVERE, null, ex);
				} catch (InstantiationException ex) { 
					Logger.getLogger(JToolBarOrnek.class.getName()).log(Level.SEVERE, null, ex); 
					} catch (IllegalAccessException ex) { 
						Logger.getLogger(JToolBarOrnek.class.getName()).log(Level.SEVERE, null, ex); 
						} catch (UnsupportedLookAndFeelException ex) {
							Logger.getLogger(JToolBarOrnek.class.getName()).log(Level.SEVERE, null, ex); 
							} 
		
		SwingUtilities.updateComponentTreeUI(f);
		
		f.setSize(300, 200);
		
		JMenuBar menuCubugu = new JMenuBar();
		
		JMenu	dosyaMenusu = new JMenu("Dosya");
		menuCubugu.add(dosyaMenusu);
		
		JMenu yardimMenusu = new JMenu("Yardım");
		menuCubugu.add(yardimMenusu);
		
		f.setJMenuBar(menuCubugu);
		
		yardimMenusu.add(new JMenuItem("Hakkında"));
		
		final Icon resim = new ImageIcon("icon/ok_kucuk.png");
		
		Action acAction = new AbstractAction() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			{
				putValue(Action.NAME, "Aç");
				putValue(Action.SMALL_ICON, resim);
			}
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Düğmeye Basıldı.");
				
			}
		};
		
		dosyaMenusu.add(acAction);
		dosyaMenusu.addSeparator();
		dosyaMenusu.add(new AbstractAction() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			{
				putValue(Action.NAME, "Çıkış");
			}
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);				
			}
		});
		
		JToolBar jtoolbar = new JToolBar();
		jtoolbar.add(acAction);
		f.add(jtoolbar, BorderLayout.NORTH);
		f.setVisible(true);

	}

}
