import java.awt.Color;
import java.awt.Component;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;

public class IconTablo {

	public static void main(String[] args) {
		JFrame f = new JFrame("İcon Tablo Örneği");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		DefaultTableModel model = new DefaultTableModel();
		JTable tablo = new JTable(model);
		
		model.addColumn("Sütun 1");
		model.addColumn("Sutun2");
		
		TableCellRenderer iconHeaderRenderer = new DefaultTableCellRenderer(){
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column){
				if(table != null){
					JTableHeader baslik = table.getTableHeader();
					if(baslik != null){
						setForeground(Color.RED);
						setBackground(baslik.getBackground());
						setFont(baslik.getFont());
					}
				}
				
				if(value instanceof MetinVeIcon){
					setIcon(((MetinVeIcon)value).icon);
					setText(((MetinVeIcon)value).metin);
				}else{
					setText((value == null) ? "" : value.toString());
					setIcon(null);
				}
				
				setBorder(UIManager.getBorder("TableHeader.cellBorder"));
				setHorizontalAlignment(JLabel.CENTER);
				
				return this;
			}
		};
		
		tablo.getTableHeader().getColumnModel().getColumn(1).setHeaderRenderer(iconHeaderRenderer);
		tablo.getColumnModel().getColumn(1).setHeaderValue(new MetinVeIcon("Sütun 2", new ImageIcon("icon/ok_kucuk.png")));
		
		f.getContentPane().add(new JScrollPane(tablo));
		f.setSize(400, 300);
		f.setVisible(true);

	}

}

class MetinVeIcon{
	
	public MetinVeIcon(String metin, Icon icon) {
		this.metin = metin;
		this.icon = icon;
	}
	
	String metin;
	Icon icon;
}
