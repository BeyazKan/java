import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTree;

public class DragDrop {

	public static void main(String[] args) {
		JFrame f = new JFrame();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setLayout(new BorderLayout());
		
		JTree agac = new JTree();
		agac.setDragEnabled(true);
		f.add(agac, BorderLayout.NORTH);
		JTextArea textArea = new JTextArea();
		f.add(new JScrollPane(textArea));
		f.setSize(300, 300);
		f.setVisible(true);
	}

}
