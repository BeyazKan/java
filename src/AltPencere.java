import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

public class AltPencere extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AltPencere(int x, int y){
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(x, y);
		Dimension boyut = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation((boyut.width - getSize().width) / 2, (boyut.height - getSize().height) / 2);
	}

	public static void main(String[] args) {
		
		AltPencere pencere = new AltPencere(400, 300);
		pencere.setVisible(true);
	}

}
