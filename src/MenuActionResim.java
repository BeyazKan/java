import java.awt.Dimension;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;

public class MenuActionResim {

	public static void main(String[] args) {
		JFrame f = new JFrame("Menülerde Resim");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JMenuBar menuCubugu = new JMenuBar();
		JMenu jmenu = new JMenu("Dosya");
		
		final Icon resim = new ImageIcon("icon/ok_kucuk.png");
		
		Action acAction = new AbstractAction() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			{
				putValue(Action.NAME, "Aç");
				putValue(Action.SMALL_ICON, resim);
			}
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Aç...");
			}
		};
		
		jmenu.add(acAction);
		
		menuCubugu.add(jmenu);
		f.setJMenuBar(menuCubugu);
		
		f.setSize(new Dimension(400, 300));
		f.setVisible(true);

	}

}
