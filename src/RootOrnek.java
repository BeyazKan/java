import java.awt.Container;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JRootPane;

public class RootOrnek {

	public static void main(String[] args) {
		JFrame f = new JFrame("RootPane Örneği");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JRootPane kok = f.getRootPane();
		Container konteyner = kok.getContentPane();
		konteyner.add(new JButton("Tamam"));
		f.pack();
		f.setVisible(true);

	}

}
