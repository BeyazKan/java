import java.awt.Dimension;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class MenuKisaTus {

	public static void main(String[] args) {
		JFrame f = new JFrame("Menü Kısayollar");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JMenuBar menuBar = new JMenuBar();
		JMenu jmenu = new JMenu("Dosya");
		
		final Icon resim = new ImageIcon("icon/ok_kucuk.png");
		
		Action cikisAction = new AbstractAction() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			{
				putValue(Action.NAME, "Çıkış");
				putValue(Action.SMALL_ICON, resim);
			}
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.exit(1);			
			}
		};
		
		JMenuItem cikisItem = new JMenuItem(cikisAction);
		
		//cikisItem.setAccelerator(KeyStroke.getKeyStroke('C', InputEvent.ALT_DOWN_MASK));
		cikisItem.setMnemonic('A');
		
		jmenu.add(cikisItem);
		
		menuBar.add(jmenu);
		f.setJMenuBar(menuBar);
		
		f.setSize(new Dimension(400, 300));
		f.setVisible(true);

	}

}
