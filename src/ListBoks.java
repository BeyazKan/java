import java.awt.BorderLayout;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JList;

public class ListBoks {

	public static void main(String[] args) {
		JFrame frame = new JFrame("ListBox Örneği");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		String[] ulke = {
			"Türkiye",
			"Almanya",
			"İngiltere",
			"Fransa",
			"Amerika",
			"Son"
		};
		
		JComponent liste = new JList(ulke);
		frame.add(liste, BorderLayout.WEST);
		
		JComboBox combo2 = new JComboBox(ulke);
		combo2.setEditable(true);
		combo2.setSelectedItem("Almanya");
		combo2.setMaximumRowCount(4);
		
		frame.add(combo2, BorderLayout.EAST);
		
		frame.pack();
		frame.setVisible(true);
	}

}
