import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class JOptionPaneOrnek {

	public static void main(String[] args) {
		JFrame f = new JFrame();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		String[] cinsiyetSecenegi = {"Erkek", "Kadın", "belirsiz", "surekli degisken"};
		
		String cinsiyet = (String) JOptionPane.showInputDialog(null, "Cinsiyet", "Lütfen Cinsiyeti Seçiniz.", 
				JOptionPane.QUESTION_MESSAGE, null, cinsiyetSecenegi, cinsiyetSecenegi[1]);
		
		System.out.println(cinsiyet);
		
		String[] evetHayirIptal = {"Evet", "Hayır", "İptal"};
		
		int n = JOptionPane.showOptionDialog(null, "Evet mi? Hayır mı?", "Evet/Hayır/İptal", 
				JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, evetHayirIptal, evetHayirIptal[0]);
		
		if(n == JOptionPane.YES_OPTION)
			System.out.println("Evet Seçildi");
		
		System.exit(0);
		
		f.setSize(200, 300);
		f.setVisible(true);
		
	}

}
