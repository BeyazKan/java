import java.awt.BorderLayout;
import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JRadioButton;

public class AlanSecimiRadio {

	public static void main(String[] args) {
		JFrame f = new JFrame();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JRadioButton radiobutton1 = new JRadioButton("siyah");
		f.add(radiobutton1, BorderLayout.NORTH);
		
		JRadioButton radiobuttom2 = new JRadioButton("beyaz");
		f.add(radiobuttom2, BorderLayout.SOUTH);
		
		radiobutton1.setSelected(true);
		ButtonGroup buttongroup = new ButtonGroup();
		buttongroup.add(radiobutton1);
		buttongroup.add(radiobuttom2);
		f.pack();
		f.setVisible(true);

	}

}
