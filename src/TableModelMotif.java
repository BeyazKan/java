import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.AbstractTableModel;

public class TableModelMotif extends AbstractTableModel{
	
	public int getRowCount(){
		return 50;
	}
	public int getColumnCount(){
		return 3;
	}
	public Object getValueAt(int satir, int sutun){
		if(sutun == 0)
			return "" + satir;
		else if(sutun == 1)
			return "" + (satir * satir);
		else
			return "" + (satir * satir * satir);
	}
	
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
		} catch (Exception e) {
			e.printStackTrace();
		}
		JFrame f= new JFrame();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.getContentPane().add(new JScrollPane(new JTable(new TableModelMotif())));
		f.pack();
		f.setVisible(true);
	}

}
