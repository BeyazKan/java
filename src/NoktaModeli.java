import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

public class NoktaModeli implements TreeModel{
	
	private final List<Point> points;
	
	public NoktaModeli(List<Point> points){
		this.points = points;
	}
	
	public Object getRoot(){
		System.out.println("Kok");
		return points;
	}
	
	public boolean isLeaf(Object node){
		System.out.printf("yaprak mi (%s)\n", node);
		return node instanceof Number;
	}
	
	public int getChildCount(Object parent){
		System.out.printf("evlat sayisi (%s)\n", parent);
		
		if(parent instanceof List)
			return ((List<?>) parent).size();
		return 2;
	}
	
	public Object getChild(Object parent, int index){
		System.out.printf("evlat( %s, %d)\n", parent, index);
		
		if(parent instanceof List)
			return ((List<?>) parent).get(index);
		if(index == 0)
			return ((Point) parent).getX();
		return ((Point) parent).getY();
	}
	
	public int getIndexOfChild(Object parent, Object child){
		return 0;
	}
	
	public void removeTreeModelListener(TreeModelListener l){}
	
	public void addTreeModelListener(TreeModelListener l){}
	
	public void valueForPathChanged(TreePath path, Object newValue){}
	

	public static void main(String[] args) {
		JFrame pencere = new JFrame();
		pencere.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		List<Point>	noktalar = new ArrayList<Point>();
		noktalar.add(new Point(12,13));
		noktalar.add(new Point(2, 123));
		noktalar.add(new Point(23, 13));
		
		JTree agac = new JTree(new NoktaModeli(noktalar));
		
		pencere.add(new JScrollPane(agac));
		pencere.pack();
		pencere.setVisible(true);
	}

}
