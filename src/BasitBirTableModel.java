import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

public class BasitBirTableModel extends AbstractTableModel{
	
	public int getRowCount(){
		return 50;
	}
	
	public int getColumnCount(){
		return 3;
	}
	
	public Object getValueAt(int satir, int sutun){
		if(sutun == 0)
			return "" + satir;
		else if(sutun == 1)
			return "" + (satir * satir);
		else
			return "" + (satir * satir * satir);
	}
	
	public boolean isCellEditable(int satir, int sutun){
		return sutun == 0;
	}
	
	public void setValueAt(Object deger, int satir, int sutun){
		Object eskideger = getValueAt(satir, sutun);
	}

	public static void main(String[] args) {
		JFrame frame = new JFrame("Abstract Table Model");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frame.getContentPane().add(new JScrollPane(new JTable(new BasitBirTableModel())));
		
		frame.pack();
		frame.setVisible(true);

	}

}
