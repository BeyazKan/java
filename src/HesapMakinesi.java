import javax.swing.JOptionPane;

public class HesapMakinesi {
	public static void main(String[] args){
		char operator = JOptionPane.showInputDialog("Operator").charAt(0);
		double x = Double.parseDouble(JOptionPane.showInputDialog("1.Sayi"));
		double y = Double.parseDouble(JOptionPane.showInputDialog("2.Sayi"));
		
		switch(operator){
		case '+':
			System.out.println(x + y);
			break;
		case '-':
			System.out.println(x - y);
			break;
		case '*':
			System.out.println(x * y);
			break;
		case '/':
			System.out.println(x / y);
			break;
		}
	}
}
