import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.AbstractAction;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

public class PopupMenuOrnegi {

	public static void main(String[] args) {
		JFrame f = new JFrame("Popup Menü Örneği");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		final JPopupMenu popup = new JPopupMenu();
		final JLabel tikla = new JLabel("Burayı Tıkla");
		
		popup.add(new JMenuItem("Bir"));
		popup.addSeparator();
		for(String s : ("İki,Üç,Dört,Beş").split(",")){
			popup.add(new AbstractAction(s) {
				
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
				public void actionPerformed(ActionEvent e) {
					tikla.setText((e.getActionCommand() + "\n"));
					
				}
			});
		}
		
		f.add(tikla);
		tikla.addMouseListener(new PopupMenuMouseListener(popup));
		
		f.setSize(300, 300);
		f.setVisible(true);

	}

}

class PopupMenuMouseListener extends MouseAdapter{
	
	private final JPopupMenu popup;
	
	public PopupMenuMouseListener(JPopupMenu popup){
		this.popup = popup;
	}
	
	public void mouseReleased(MouseEvent me){
		popup.show(me.getComponent(), me.getX(), me.getY());
	}
	
}
