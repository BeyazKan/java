import java.awt.Dimension;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;

public class MenuAction {

	public static void main(String[] args) {
		JFrame f = new JFrame("Menülerde Action");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JMenuBar menuCubugu = new JMenuBar();
		JMenu jmenu = new JMenu("Dosya");
		
		Action cikAction = new AbstractAction("Çık") {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent e) {
				System.exit(0);				
			}
		};
		
		jmenu.add(cikAction);
		
		menuCubugu.add(jmenu);
		f.setJMenuBar(menuCubugu);
		
		f.setSize(new Dimension(400, 300));
		f.setVisible(true);

	}

}
