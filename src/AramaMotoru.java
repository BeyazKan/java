import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Scanner;

public class AramaMotoru {
	public static void main(String[] args){
		try{
			String arananKelime = "kitap";
				if(args.length > 0){
					arananKelime = args[0];
					for(int i = 1; i < args.length; i++)
						arananKelime += " " + args[i];
				}
				arananKelime = "p=" + URLEncoder.encode(arananKelime.trim(), "UTF-8");
				URL urlObjesi = new URL("http://www.google.com.tr/search?" + arananKelime);
				
				@SuppressWarnings("resource")
				String cikti = new Scanner(urlObjesi.openStream()).useDelimiter("\\Z").next();
				System.out.println(cikti);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
