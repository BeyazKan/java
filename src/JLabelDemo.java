import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class JLabelDemo {
	public static void main(String[] args){
		JFrame jframe = new JFrame("Deneme");
		jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JLabel jlabel = new JLabel("Burayı çift tıklarsan program biter");
		jlabel.setForeground(Color.RED);
		jlabel.setFont(new Font("Serif", Font.BOLD, 30));
		jframe.add(jlabel);
		
		jlabel.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e){
				if(e.getClickCount() > 1)
					System.exit(0);
			}
		});
		
		jframe.pack();
		jframe.setVisible(true);
	}
}
