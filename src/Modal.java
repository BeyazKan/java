import javax.swing.JDialog;
import javax.swing.JFrame;

public class Modal {

	public static void main(String[] args) {
		JFrame f = new JFrame();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setSize(200, 300);
		f.setVisible(true);
		f.setTitle("Ana Pencere");
		
		JDialog d = new JDialog(f, true);
		d.setSize(100, 100);
		d.setTitle("Modal");
		d.setVisible(true);

	}

}
