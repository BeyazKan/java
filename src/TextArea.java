import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToggleButton;

public class TextArea extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Font font = new Font("Times New Roman", Font.PLAIN, 15);
	
	public TextArea(){
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		final JTextArea textarea = new JTextArea();
		textarea.setFont(font);
		add(new JScrollPane(textarea));
		
		ActionListener al = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if("Son".equals(e.getActionCommand()))
					System.exit(0);
				if("Kalin".equals(e.getActionCommand()))
					textarea.setFont(font = font.deriveFont(font.getStyle() ^ Font.BOLD));
				else if("Yatay".equals(e.getActionCommand()))
					textarea.setFont(font = font.deriveFont(font.getSize() ^ Font.ITALIC));
			}
		};
		
		JPanel panel = new JPanel(new GridLayout(1, 3));
		add(panel, BorderLayout.PAGE_START);
		
		AbstractButton button;
		panel.add(button = new JToggleButton("Kalin"));
		button.addActionListener(al);
		button.setFont(font.deriveFont(Font.BOLD));
		
		panel.add(button = new JToggleButton("Yatay"));
		button.addActionListener(al);
		button.setFont(font.deriveFont(Font.ITALIC));
		
		panel.add(button = new JButton("Son"));
		button.addActionListener(al);
		setSize(400, 600);
		
	}
	
	public static void main(String[] args) {
		new TextArea().setVisible(true);

	}

}
