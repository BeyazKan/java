import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

public class BasitBirAgac {

	public static void main(String[] args) {
		JFrame pencere = new JFrame("JTree (Ağaç) Örneği");
		pencere.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		DefaultMutableTreeNode kok = new DefaultMutableTreeNode("Kök");
		for(int kokSayac = 0; kokSayac < 4; kokSayac++){
			DefaultMutableTreeNode dmtn = new DefaultMutableTreeNode("Düğüm " + kokSayac);
			kok.add(dmtn);
			for(int yaprakSayac = 1; yaprakSayac < 4; yaprakSayac++){
				dmtn.add(new DefaultMutableTreeNode("Yaprak " + (kokSayac * 3 + yaprakSayac)));
			}
		}
		
		JTree agac = new JTree(kok);
		pencere.add(new JScrollPane(agac));
		pencere.pack();
		pencere.setVisible(true);
		
		agac.getSelectionModel().addTreeSelectionListener(new TreeSelectionListener() {
			
			@Override
			public void valueChanged(TreeSelectionEvent e) {
				TreePath dizin = e.getNewLeadSelectionPath();
				System.out.println(dizin);
				
			}
		});

	}

}
