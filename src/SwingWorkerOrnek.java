import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.SwingWorker;

public class SwingWorkerOrnek extends JFrame{
	JButton dugme = new JButton("Buraya Sürekli Tıkla");
	
	public SwingWorkerOrnek() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		add(dugme);
		ActionListener al = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				new Saat().execute();
				
			}
		};
		
		dugme.addActionListener(al);
		pack();
	}
	
	class Saat extends SwingWorker<Long, Object>{
		public Long doInBackground(){
			long nanoSaniyeBaslat = System.nanoTime();
			
			try {
				Thread.sleep(300);
			} catch (InterruptedException e) {
			}
			
			return (System.nanoTime() - nanoSaniyeBaslat) / (1000 * 1000);
		}
		
		protected void done(){
			try {
				dugme.setText("" + get());
			} catch (Exception e) {
			}
		}
	}
	
	public static void main(String[] args){
		new SwingWorkerOrnek().setVisible(true);
	}

}
